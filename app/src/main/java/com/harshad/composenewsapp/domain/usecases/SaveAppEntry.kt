package com.harshad.composenewsapp.domain.usecases

import com.harshad.composenewsapp.domain.manager.LocalUserManager

class SaveAppEntry(private val localUserManager: LocalUserManager) {

    suspend operator fun invoke() {
     localUserManager.saveAppEntry()
    }
}