package com.harshad.composenewsapp.domain.usecases

import com.harshad.composenewsapp.domain.manager.LocalUserManager
import kotlinx.coroutines.flow.Flow

class ReadAppEntry(private val localUserManager: LocalUserManager) {

     operator fun invoke(): Flow<Boolean> {
      return localUserManager.readAppEntry()
    }
}