package com.harshad.composenewsapp.presentation.onboarding

sealed class OnBoardingEvent {

    object SaveAppEntry : OnBoardingEvent()

}