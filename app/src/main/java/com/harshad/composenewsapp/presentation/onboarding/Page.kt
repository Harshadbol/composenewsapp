package com.harshad.composenewsapp.presentation.onboarding

import androidx.annotation.DrawableRes
import com.harshad.composenewsapp.R

data class Page(
    val title: String,
    val description: String,
    @DrawableRes val banner: Int
)

val pages = listOf(
    Page(
        title = "A Fresh Look at Thai Food at Bangkok Supper Club",
        description = "Max Wittawat was a pastry chef at Spot Dessert Bar for 13 years, and sometimes you can see the imprint of that work in the Thai food he serves now as the chef of Bangkok Supper Club, a new restaurant in the West Village.",
        banner = R.drawable.news_onboard1
    ),
    Page(
        title = "Apple Is Doing Its Part to End Green Bubble Shaming. It’s Our Turn",
        description = "For more than a decade, smartphone users everywhere have faced a major problem in how we communicate: the “green versus blue bubble” disparity.",
        banner = R.drawable.news_onboard2
    ),
    Page(
        title = "Google Updates Bard Chatbot With ‘Gemini’ A.I. as It Chases ChatGPT",
        description = "For more than a year, Google has raced to build technology that could match ChatGPT, the eye-opening chatbot offered by the San Francisco artificial intelligence start-up OpenAI.",
        banner = R.drawable.news_onboard3
    )
)
