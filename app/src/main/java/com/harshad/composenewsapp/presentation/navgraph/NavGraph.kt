package com.harshad.composenewsapp.presentation.navgraph

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navigation
import com.harshad.composenewsapp.presentation.onboarding.OnBoardingScreen
import com.harshad.composenewsapp.presentation.onboarding.OnBoardingViewModel

@Composable
fun NavGraph(startDestination: String) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = startDestination) {
        //OnBoarding flow
        navigation(
            route = Route.AppStartNavigation.route,
            startDestination = Route.OnBoardingScreen.route
        ) {
            composable(route = Route.OnBoardingScreen.route) {
                GetOnBoardScreen()
            }
        }

        //NewsNavigation flow
        navigation(
            route = Route.NewsNavigation.route,
            startDestination = Route.NewsNavigatorScreen.route
        ) {
            composable(route = Route.NewsNavigatorScreen.route) {
                Text(text = "News Navigator Screen")
            }
        }
    }
}

@Composable
fun GetOnBoardScreen() {
    val viewModel: OnBoardingViewModel = hiltViewModel()
    //viemodel:: is shor of viewmodel{onEvent(it)}
    OnBoardingScreen(event = viewModel::onEvent)
}
