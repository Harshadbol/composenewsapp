package com.harshad.composenewsapp.presentation.onboarding.component

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.harshad.composenewsapp.R
import com.harshad.composenewsapp.presentation.Dimens.BannerPadding
import com.harshad.composenewsapp.presentation.Dimens.TitlePadding
import com.harshad.composenewsapp.presentation.onboarding.Page
import com.harshad.composenewsapp.presentation.onboarding.pages
import com.harshad.composenewsapp.ui.theme.ComposeNewsAppTheme

@Composable
fun OnBoardingPage(page: Page) {
    Column(modifier = Modifier) {
        Image(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(fraction = 0.4f),
            painter = painterResource(id = page.banner),
            contentDescription = "onboard banner",
            contentScale = ContentScale.Crop,
        )
        Spacer(modifier = Modifier.height(BannerPadding))

        Text(
            text = page.title, modifier = Modifier.padding(TitlePadding),
            style = MaterialTheme.typography.headlineMedium.copy(
                fontWeight = FontWeight.Bold,
                fontStyle = FontStyle(R.font.poppins_bold)
            ),
            color = colorResource(id = R.color.display_small)
        )

        Text(
            text = page.description, modifier = Modifier.padding(TitlePadding),
            style = MaterialTheme.typography.bodyMedium.copy(fontStyle = FontStyle(R.font.poppins_regular)),
            color = colorResource(id = R.color.text_medium)
        )
    }
}

@Preview(showBackground = true)
@Preview(uiMode = UI_MODE_NIGHT_YES,showBackground = true)
@Composable
fun OnBoardingPreview() {
    ComposeNewsAppTheme {
        OnBoardingPage(page = pages[0])
    }
}