package com.harshad.composenewsapp.presentation

import androidx.compose.ui.unit.dp

object Dimens {
    val BannerPadding = 18.dp
    val TitlePadding = 10.dp
    val IndicatorSize = 14.dp
    val MediumPadding = 30.dp
    val PageIndicatorWidth = 52.dp
}