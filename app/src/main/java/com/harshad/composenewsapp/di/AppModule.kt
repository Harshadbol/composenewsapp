package com.harshad.composenewsapp.di

import android.app.Application
import com.harshad.composenewsapp.data.manager.LocalUserManagerImp
import com.harshad.composenewsapp.domain.manager.LocalUserManager
import com.harshad.composenewsapp.domain.usecases.AppEntryUseCases
import com.harshad.composenewsapp.domain.usecases.ReadAppEntry
import com.harshad.composenewsapp.domain.usecases.SaveAppEntry
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideLocalUserManager(application: Application): LocalUserManager =
        LocalUserManagerImp(application)

    @Provides
    @Singleton
    fun provideAppEntryUseCases(localUserManager: LocalUserManager) = AppEntryUseCases(
        readAppEntry = ReadAppEntry(localUserManager),
        saveAppEntry = SaveAppEntry(localUserManager)
    )


}